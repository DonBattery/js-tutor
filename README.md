# js-tutor

**Egyszerű JavaScript Fórum**

## Használat

Klónozd meg a repót (jobb felső sarok kis kék clone gomb -> clone with HTTPS copy URL -> GitBash ban navigálj a dokumentumaidhoz -> git clone SHIFT+Insert)

Szedjük le a NodeJs-t [Node.js](https://nodejs.org/en/)

Ha minden jól ment a GitBash-ban ha beírod, hogy `node` akkor el kell indulnia az interpreternek. Ez hasonló a Python interaktív interpreteréhez, ahol soronként adhatunk utasításokat és minden enter után a Python megpróbálja értelmezni az utasítást. Ugyan olyan mint amikot a böngészőben megnyitjuk a konzolt, és közvetlenül adunk ki utasításokat, azzal a külömbséggel, hogy itt se document, se window nincsen hiszen nem a böngészőben vagyunk hanem a terminálban! A CONTROL+c ismételt lenyomásával léphetünk ki node interpreterből.

Ha készítünk egy sima kis script file-t `test-script.js`
```JAVASCRIPT
"use strict";

console.log("Hello form scriptfile");

```
és a GitBash-ba beírjuk a `node test-script.js` parancsot, akkor a NodeJs megfuttatja a fájlunkat. A NodeJs képessé tesz bennünket, hogy a böngészőtől teljesen függetlenül JavaScript nyelven írt programokat futtassunk. Jellemzően ezek szerver oldali alkalmazások, amik nagyon jól együt tudnak működni weblapokkal, hiszen azokon alapnól JavaScript fut...


Ha belépünk a js-tutor mappába, amit az imént klónoztunk meg a dokumentumainkba. És kiadjuk az `npm install` parancsot, akkor az npm (a NodeJs csomagkezelője, hasonló a Python pip -hez) leszedi a szükséges csomagokat a node_modules mappába (ezekre a szervernek lesz szüksége). Figyeljük meg, hogy a .gitignore file-ban ignoráltam a node_modules mappát, azért tettem, hogy ne töltsem fel a saját repom-ba a sok függőségemet (ezeket az npm-el tudom letölteni vagy frissíteni ha kell).

Ha kiadjuk az `npm start` parancsot, akkor megindul a szerver (ez maga a Fórum API), innentől kezdve él a fórum, a client nevű mappában található meg hozzá a weboldal, amin el lehet érni.

## Feladat

Az [express.static](https://expressjs.com/en/starter/static-files.html) segítségével hosztoljuk ki magát az oldalt a "/" root-on, jelenleg az csak annyit mond, hogy Forum API.

Találjuk meg a gépünk ip -címét az ipconfig segítségével, és próbáljuk meg megnyitni az oldalunkat egy másik eszköztről (telefon, számítógép).

## Hasznos oldalak

[Node.js](https://nodejs.org/en/)

[Express](https://expressjs.com/)

[HTTP.CAT](https://http.cat/)

[Postman](https://www.getpostman.com/downloads/)
