'use strict';

const ForumAPI = 'http://localhost:8081/';

function myRequest(method = 'GET', type = 'text', url = '', parameters = [], querys = {}, body = null, readyCallback = () => {}, errorCallback = () => {}, expectedStatus = [200]) {
    const reqURL = url.concat('/', parameters.join('/'), (querys.length > 0) ? '?'.concat(Object.keys(querys).map(key => key.concat('=', encodeURIComponent(querys[key]))).join('&')) : '');
    const myRequest = new XMLHttpRequest();

    myRequest.open(method, reqURL);

    if (type === 'JSON') {
     myRequest.setRequestHeader('Content-Type', 'application/json');
     myRequest.setRequestHeader('Accept', 'application/json');
    }

    myRequest.onreadystatechange = () => {
      if (myRequest.readyState === 4) {
        if (expectedStatus.includes(myRequest.status)) {
          readyCallback((type == 'JSON') ? JSON.parse(myRequest.responseText) : myRequest.responseText);
          return
        };
        let errorJSON = { status: myRequest.statusText, code: myRequest.status, text: myRequest.responseText };     
        let errorText = `Status: ${myRequest.statusText} Code: ${myRequest.status} Text: ${myRequest.responseText}`;
        errorCallback((type == 'JSON') ? errorJSON : errorText);
      };
    };

    if (type === 'JSON') {
      body = JSON.stringify(body);
    };

    myRequest.send(body);
}

function GET(url, readyCallback = () => {}, errorCallback = () => {}) {
  myRequest('GET', 'JSON', url, [], {}, null, readyCallback, errorCallback)
}

function POST(url, body = null, readyCallback = () => {}, errorCallback = () => {}) {
  myRequest('POST', 'JSON', url, [], {}, body, readyCallback, errorCallback)
}

function ForumPost(author, content) {
  let thread = document.getElementById('Thread');
  let newPost = document.createElement('dt');
  newPost.innerText = `${author} : ${content}`;
  thread.appendChild(newPost);
  let items = document.querySelectorAll('dt');
  let lastItem = items[items.length - 1];
  lastItem.scrollIntoView();
}

function OnSendClick() {
  let author = document.getElementById('UserName').value;
  let content = document.getElementById('UserPost').value;
  ForumPost(author, content);
  POST(ForumAPI + 'post', {Author: author, Content: content}, console.log, console.log);
}

function PopulateThread(posts) {
  let thread = document.getElementById('Thread');
  thread.innerText = '';
  for (let index = 0; index < posts.length; index++) {
    const element = posts[index];
      ForumPost(element.Author, element.Content);
  }
}

function SetUpPage() {
  let sendButton = document.getElementById('SendButton');
  sendButton.addEventListener('click', OnSendClick)

  GET(ForumAPI + 'posts', PopulateThread, console.log);
}

window.addEventListener('load', SetUpPage);
