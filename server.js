'use strict';

// Behozom az Express framework-öt, nézetek utána Express Js
const Express = require("express");
// Erre azért lesz szüség, hogy mások is elérjék az API-t
const Cors = require('cors');
// Ez egy loggoló middleware, ez fogja kiírni, hogy milyen kérések jöttek aszerverre
const Morgan = require('morgan');

// Csinálok egy példányt az Express -ből
const App = Express();
// Ezen a port -on fog figyelni a szerver
const PORT = "8081";

// Így fog kinézni egy fórum poszt
class ForumPost {
  constructor(Author, Content) {
    this.Author = Author;
    this.Content = Content;
  };
};

// Ez egy thread, ami fórumposztokból áll
class Thread {
  constructor() {
    this.posts = [];

    // Így aggathatok metódusokat egy objektumra, itt azt mondom neki, hogy a this jelentse
    // pontosan ezt az objektumot, amikor meghívják rajta az addPost vagy a getJSON metódusokat.
    this.addPost = this.addPost.bind(this);
    this.getJSON = this.getJSON.bind(this);
  };

  // Hozzáad egy post-ot a thread-hez
  addPost(Author, Content) {
    let post = new ForumPost(Author, Content);
    this.posts.push(post);
  };

  // JSON formátumban adja vissza az összes post-ot a thread-ben,
  // Erre azért van szükség, mert a HTTP body-jába JSON-t rakhatunk, de sima JavaScript objektumot nem
  getJSON () {
    return JSON.stringify(this.posts);
  };
};

// Létrehozok egy darab thread-et, ez lesz maga a fórum
var mainThread = new Thread();

// Az App.use segítségével adhatok middleware-ket a szerveremhez, ezek minden egyes
// requestnél (kérés) lefutnak és csinálnak valamit, ez a middleware például kiloggolja
// a konzolra a requestet
App.use(Morgan('combined'));

// Ez a middleware teszi lehetővé hogy ne csak azokról az oldalakról lehessen kérést intézni
// a szerver felé, amiket ő maga hosztolt ki (ne feledjük a kliensünk egyelőre a file:// protokolt használja).
App.use(Cors());

// Ez a middleware teszi lehetővé, hogy a request body-jában lévő JSON objektumhoz hozzáférjünk
// Esetünkben az Author és a Content tulajdonságokhoz.
App.use(Express.json());

// Az App.get egy GET típusú kérést kezel, ha egy App.get már lekezelt egy kérést, akkor azt nem fogja
// más App.get kezelni, ezért fontos, hogy milyen sorrendben írjuk őket le.
// a res.send() metódus visszaküld egy választ a kliensnek 200 as HTTP kóddal
App.get("/", (req, res) => {
  res.send("Forum API\n");
});

// Ez a route, vagy más néven endpoint visszaadja az egész thread-et, JSON formában küldjük el, így
// A másik kliens könnyen vissza alakíthatja JavaScript objektummá
App.get("/posts", (req, res) => {
  res.send(mainThread.getJSON());
});

// Az App.post hasonlít az App.get-re de ez POST típusú HTTP kéréseket kezel
// A req.body -ban van a kliens által posztolt tartalom,
// mivel használjuk az Express.json()-t közvetlenül elérhetjük a req.body.Content
// és req.body.Author property-ket
App.post("/post", (req, res) => {
  // Biztos ami boztos megnézem, hogy a req.body -ban, benne van-e az Author és a Content
  if (req.body.hasOwnProperty("Author") && req.body.hasOwnProperty("Content")) {
    // Ha igen berakom a mainThread-be
    mainThread.addPost(req.body.Author, req.body.Content);
    res.status(201);
    res.send("Post created\n");
  } else {
    // Ha nem, hibát küldök vissza
    res.status(400);
    res.send("Bad request\n");
  };

});

// Ez az endpoint el fog kapni minden más GET requestet és a 404-es hibával fog válaszolni.
App.get("*", (req, res) => {
  res.status(404);
  res.send("Not found\n");
});

// Ez a sor indítja meg magát a szervert, az első paraméter a port, amin a szerver figyelni fog
// a második paraméter egy fügvény ami akkor fog lefutni ha sikeresen elindult a szerver.
// Én itt kiírok a onzolra egy üzenetet benne a PORT számával, ez jó gyakorlat
App.listen(PORT, () => {console.log(`web-tutor server listening on PORT ${PORT}`)});
